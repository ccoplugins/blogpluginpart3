package com.be1eye.cco.blogpluginpart3;

import java.math.BigDecimal;

import com.be1eye.cco.blogpluginpart3.dao.LoyaltyFactorDao;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorDto;
import com.sap.scco.ap.pos.entity.ReceiptEntity;
import com.sap.scco.ap.pos.entity.SalesItemEntity;
import com.sap.scco.ap.pos.loyalty.PointCalculationLogic;

public class LoyaltyPointCalculation implements PointCalculationLogic {


	public BigDecimal calculatePointValue(ReceiptEntity receipt) {
		
		BigDecimal overallAmount = BigDecimal.ZERO;

		
		// get the overall amount for this receipt
		for(SalesItemEntity item : receipt.getSalesItems()) {
			overallAmount = overallAmount.add(item.getLoyaltyPoints());
		}
		
		// if a businesspartner is set, get the factor and multiply
		if(receipt.getBusinessPartner() != null) {
			
			LoyaltyFactorDao loyaltyFactorDao = new LoyaltyFactorDao();
			
			// get the loyaltyFactor for this business partner
			LoyaltyFactorDto loyaltyFactorDto = loyaltyFactorDao.findOne(receipt.getBusinessPartner().getExternalId());
			
			overallAmount = overallAmount.multiply(loyaltyFactorDto.getLoyaltyFactor());
		}
		
		return overallAmount;
	}

	public BigDecimal calculatePointValue(SalesItemEntity salesItem) {
		return SalesItemEntity.SalesItemTypeCode.MATERIAL.equals(salesItem.getTypeCode()) ? salesItem.getPaymentGrossAmount() : BigDecimal.ONE;
	}

	public void distributePointsToItems(ReceiptEntity arg0) {

	}

	public String getId() {
		return "LoyaltyPointCalc";
	}

}
