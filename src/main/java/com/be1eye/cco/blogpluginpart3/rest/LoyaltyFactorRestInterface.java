package com.be1eye.cco.blogpluginpart3.rest;

import java.util.List;

import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorRequestDto;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorResponseDto;

import feign.RequestLine;

public interface LoyaltyFactorRestInterface {
	
	@RequestLine("POST /B1iXcellerator/exec/ipo/vP.001sap0013.in_HCSX/com.sap.b1i.vplatform.runtime/INB_HT_CALL_SYNC_XPT/INB_HT_CALL_SYNC_XPT.ipo/proc?action=getLoyaltyFactor&bpm.pltype=json")
	List<LoyaltyFactorResponseDto> requestLoyaltyData(LoyaltyFactorRequestDto request); 

}
