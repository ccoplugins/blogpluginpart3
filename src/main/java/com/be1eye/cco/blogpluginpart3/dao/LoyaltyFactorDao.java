package com.be1eye.cco.blogpluginpart3.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorDto;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.util.logging.Logger;

/**
 * 
 * @author RobertZieschang zieschang@be1eye.de
 *
 */
public class LoyaltyFactorDao {

	// use the logger from cco
	private static final Logger logger = Logger.getLogger(LoyaltyFactorDao.class);

	// name our table
	private static final String TABLE_NAME = "B1E_LOYALTYFACTOR";

	// create query
	private static final String QUERY_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
			+ "CARDCODE varchar(20) not null PRIMARY KEY," 
			+ "LOYALTYFACTOR decimal(18,4) not null)";

	// query for adding
	private static final String QUERY_INSERT_ROW = "INSERT INTO " + TABLE_NAME + " VALUES(?,?)";

	// query for updating
	private static final String QUERY_UPDATE_ROW = "UPDATE " + TABLE_NAME
			+ " SET LOYALTYFACTOR = ?2 WHERE CARDCODE = ?1";

	// query for find all
	private static final String QUERY_FIND_ALL = "SELECT CARDCODE, LOYALTYFACTOR FROM " + TABLE_NAME;

	// query for find one
	private static final String QUERY_FIND_ONE = "SELECT CARDCODE, LOYALTYFACTOR FROM " + TABLE_NAME
			+ " where CARDCODE = ?";

	// query to drop one
	private static final String QUERY_DROP_ONE = "DELETE FROM " + TABLE_NAME + " WHERE CARDCODE = ?";

	/**
	 * Create the table if not existing
	 */
	public void setupTable() {
		// lets open a session
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();

			Query q = em.createNativeQuery(QUERY_CREATE_TABLE);
			q.executeUpdate();

			session.commitTransaction();
			logger.info("Created table " + TABLE_NAME);

		} catch (Exception e) {
			session.rollbackDBSession();
			logger.info("Error or table " + TABLE_NAME + " already existing");
		} finally {
			session.closeDBSession();
		}

	}

	/**
	 * Create or update an entry in our table.
	 * 
	 * @param loyaltyFactor
	 */
	private void save(LoyaltyFactorDto loyaltyFactor, boolean isAlreadyInDB) {

		CDBSession session = CDBSessionFactory.instance.createSession();
		String query = isAlreadyInDB ? QUERY_UPDATE_ROW : QUERY_INSERT_ROW;

		try {
			session.beginTransaction();
			EntityManager em = session.getEM();

			Query q = em.createNativeQuery(query);
			q.setParameter(1, loyaltyFactor.getCardCode());
			q.setParameter(2, loyaltyFactor.getLoyaltyFactor().doubleValue());

			q.executeUpdate();
			session.commitTransaction();

		} catch (Exception e) {
			session.rollbackDBSession();
			logger.info("Could not create LoyaltyFactor");
			logger.info(e.getLocalizedMessage());
		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Get all Customer LoyaltyFactors
	 * 
	 * @return
	 */
	public List<LoyaltyFactorDto> findAll() {

		CDBSession session = CDBSessionFactory.instance.createSession();
		ArrayList<LoyaltyFactorDto> resultList = new ArrayList<LoyaltyFactorDto>();

		try {
			session.beginTransaction();
			EntityManager em = session.getEM();

			Query q = em.createNativeQuery(QUERY_FIND_ALL);
			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();

			if (results.isEmpty()) {
				// return an empty list rather than null
				return resultList;
			}

			for (Object[] resultRow : results) {
				resultList.add(new LoyaltyFactorDto((String) resultRow[0], (BigDecimal) resultRow[1]));
			}

		} catch (Exception e) {
			logger.info("Error while getting results from table " + TABLE_NAME);

		} finally {
			session.closeDBSession();

		}

		return resultList;

	}

	/**
	 * Get one loyaltyfactor
	 * 
	 * @param cardCode
	 * @return
	 */
	public LoyaltyFactorDto findOne(String cardCode) {

		CDBSession session = CDBSessionFactory.instance.createSession();
		LoyaltyFactorDto loyaltyFactor = new LoyaltyFactorDto();

		try {

			EntityManager em = session.getEM();
			Query q = em.createNativeQuery(QUERY_FIND_ONE);
			q.setParameter(1, cardCode);

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();

			if (results.isEmpty()) {
				// return empty dto
				return loyaltyFactor;
			}

			loyaltyFactor.setCardCode((String) results.get(0)[0]);
			loyaltyFactor.setLoyaltyFactor((BigDecimal) results.get(0)[1]);

		} catch (Exception e) {
			logger.info("Error while getting " + cardCode + " from table " + TABLE_NAME);

		} finally {
			session.closeDBSession();
		}

		return loyaltyFactor;
	}

	/**
	 * Drop one loyalty factor
	 * 
	 * @param cardCode
	 */
	public void dropOne(String cardCode) {
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery(QUERY_DROP_ONE);
			q.setParameter(1, cardCode);
			q.executeUpdate();
			session.commitTransaction();

		} catch (Exception ex) {

		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Drop all loyaltyfactors
	 */
	public void dropAll() {
		List<LoyaltyFactorDto> list = this.findAll();

		for (LoyaltyFactorDto entry : list) {
			this.dropOne(entry.getCardCode());
		}
	}

	/**
	 * Save a loyaltyFactor
	 * 
	 * @param loyaltyFactor
	 * @return
	 */
	public void save(LoyaltyFactorDto loyaltyFactor) {
		LoyaltyFactorDto loyaltyInDB = this.findOne(loyaltyFactor.getCardCode());
		boolean isAlreadyInDb = loyaltyFactor.getCardCode().equals(loyaltyInDB.getCardCode());
		// check if entity is already in database, so that we update rather than insert.
		logger.info("Trying to save " + loyaltyFactor.toString() + " and " + isAlreadyInDb);
		this.save(loyaltyFactor, isAlreadyInDb);

	}
}
