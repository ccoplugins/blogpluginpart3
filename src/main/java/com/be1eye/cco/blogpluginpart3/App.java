package com.be1eye.cco.blogpluginpart3;


import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.util.StringUtils;

import com.be1eye.cco.blogpluginpart3.dao.LoyaltyFactorDao;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorDto;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorRequestDto;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorResponseDto;
import com.be1eye.cco.blogpluginpart3.rest.LoyaltyFactorRestInterface;
import com.sap.scco.ap.configuration.ConfigurationHelper;
import com.sap.scco.ap.plugin.BasePlugin;
import com.sap.scco.ap.plugin.annotation.PluginAt;
import com.sap.scco.ap.plugin.annotation.PluginAt.POSITION;
import com.sap.scco.ap.plugin.annotation.Schedulable;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.ap.pos.dao.IReceiptManager;
import com.sap.scco.ap.pos.dao.SynchStatusManager;
import com.sap.scco.ap.pos.entity.BusinessPartnerEntity;
import com.sap.scco.ap.pos.i14y.util.context.I14YContext;
import com.sap.scco.ap.pos.job.PluginJob;
import com.sap.scco.ap.pos.service.LoyaltyPosService;
import com.sap.scco.ap.pos.service.impl.LoyaltyPosServiceImpl;
import com.sap.scco.ap.pos.util.TriggerParameter;
import com.sap.scco.util.logging.Logger;

import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import feign.httpclient.ApacheHttpClient;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

/**
 * @author RobertZieschang zieschang@be1eye.de
 *
 */
public class App extends BasePlugin {
	
	private static final Logger logger = Logger.getLogger(App.class);
	
	private LoyaltyFactorDao loyaltyFactorDao;
	
	private LoyaltyFactorRestInterface restInterface;
	
	private String sysId;

	@Override
	public String getId() {
		return "pluginpartthree";
	}

	@Override
	public String getName() {
		return "CCO Plugin Part 3";
	}

	@Override
	public String getVersion() {
		return getClass().getPackage().getImplementationVersion();
	}

	@Override 
	public void startup() {
		
		// setting up the local user-defined-table
		loyaltyFactorDao = new LoyaltyFactorDao();
		loyaltyFactorDao.setupTable();
		
		// getting the host and port of our b1i server
		String b1iHost = ConfigurationHelper.INSTANCE.getB1ServiceDestination().getHost();
		int b1iPort = ConfigurationHelper.INSTANCE.getB1ServiceDestination().getPort();
		
		// getting the http basic auth credentials
		String httpBasicAuthUser = ConfigurationHelper.INSTANCE.getB1ServiceDestination().getUserName();
		String httpBasicAuthPassword = new String(ConfigurationHelper.INSTANCE.getB1ServiceDestination().getPassword());
		
		// use the string builder to create host + port
		StringBuilder sb = new StringBuilder();
		sb.append(b1iHost);
		sb.append(":");
		sb.append(b1iPort);
		
		// get the sysid
		this.sysId = ConfigurationHelper.INSTANCE.getB1IntegrationSettings().getSystemId();
		
		// setting up our REST Interface
		restInterface = Feign.builder()
				.client(new ApacheHttpClient())
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.requestInterceptor(new BasicAuthRequestInterceptor(httpBasicAuthUser, httpBasicAuthPassword))
				.target(LoyaltyFactorRestInterface.class, sb.toString());
		
		CDBSession session = CDBSessionFactory.instance.createSession();
		
		try {
			LoyaltyPosService loyaltyPosService = new LoyaltyPosServiceImpl(session);
			loyaltyPosService.registerPointCalculationLogic(new LoyaltyPointCalculation());
		} catch (Exception e) {
			logger.info(e.getMessage());
		} finally {
			session.closeDBSession();
		}
		

		
		super.startup();

	}
	
	@PluginAt(pluginClass = IReceiptManager.class, method ="setBusinessPartner", where = POSITION.AFTER)
	public Object checkForLoyaltyFactor(Object proxy, Object[] args, Object ret, StackTraceElement caller) {
		
		try {
			BusinessPartnerEntity bp = (BusinessPartnerEntity) args[1];
			
			LoyaltyFactorDto loyaltyFactorDto = this.loyaltyFactorDao.findOne(bp.getExternalId());
			
			// If cardcode is empty, then it is not in the database...
			if(StringUtils.isEmpty(loyaltyFactorDto.getCardCode())) {
				LoyaltyFactorRequestDto request = new LoyaltyFactorRequestDto();
				request.setSysId(this.sysId);
				request.setCardCode(bp.getExternalId());
				
				List<LoyaltyFactorResponseDto> response = restInterface.requestLoyaltyData(request);
				if(!response.isEmpty()) {
					this.loyaltyFactorDao.save(new LoyaltyFactorDto((String) response.get(0).getCardCode(), new BigDecimal(response.get(0).getULoyFac())));
				}			
			}
		} catch (Exception e) {
			logger.info(e.getLocalizedMessage());
		}

		
		return ret;
	}
	
	
	
	@Schedulable("LoyaltyFactor Sync")
	public void syncLoyaltyFactor(PluginJob job, TriggerParameter triggerParam, I14YContext context, Object[] params){

		// We get the SyncStatusmanager from the database
		CDBSession session = CDBSessionFactory.instance.createSession();
		SynchStatusManager syncStatusManager = new SynchStatusManager(session);
		Date lastUpdate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// We use the customerquerymax results for this. Maybe a plugin configuration would also be suitable.
		Integer batchSize = ConfigurationHelper.INSTANCE.getB1IntegrationSettings().getCustomerQueryMaxResults();
		
		// Is it a fully snyc?
		if(triggerParam.isCleanSync())
		{
			try {
				lastUpdate = dateFormat.parse("1970-01-01");
			} catch (ParseException e) {
				logger.severe(e.getLocalizedMessage());
			}
		}
		else
		{
			lastUpdate = syncStatusManager.readLastUpdateOfObject(PluginJob.createObjectId(triggerParam.getPluginId(), triggerParam.getPluginMethodId()));
		}
		
		// Create the sync thread.
		try {
			ExecutorService executor = Executors.newCachedThreadPool();
			executor.execute(new LoyaltyFactorSyncThread(this.restInterface, this.sysId, dateFormat.format(lastUpdate), batchSize));
			
		} catch (Exception e) {
			logger.info("Error while sync");
			
		} finally {
			session.closeDBSession();
		}		
	}

}
