package com.be1eye.cco.blogpluginpart3;

import java.math.BigDecimal;
import java.util.List;

import com.be1eye.cco.blogpluginpart3.dao.LoyaltyFactorDao;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorDto;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorRequestDto;
import com.be1eye.cco.blogpluginpart3.dto.LoyaltyFactorResponseDto;
import com.be1eye.cco.blogpluginpart3.rest.LoyaltyFactorRestInterface;

public class LoyaltyFactorSyncThread implements Runnable {
	
	private LoyaltyFactorRestInterface restClient;
	
	private LoyaltyFactorDao loyaltyFactorDao;
	
	private String sysId;
	
	private String lastUpdate;
	
	private Integer messageSize;
	
	private String lastReturnedObjectId = "0";
	
	public LoyaltyFactorSyncThread(LoyaltyFactorRestInterface restClient, String sysId, String lastUpdate, Integer messageSize) {
		this.restClient = restClient;
		this.sysId = sysId;
		this.lastUpdate = lastUpdate;
		this.messageSize = messageSize;	
		this.loyaltyFactorDao = new LoyaltyFactorDao();
		
	}

	public void run() {
		boolean syncComplete = false;
		do {
			LoyaltyFactorRequestDto request = new LoyaltyFactorRequestDto();
			request.setLastModifiedDate(this.lastUpdate);
			request.setSysId(this.sysId);
			request.setQueryHitsMaximumValue(this.messageSize.toString());
			request.setLastReturnedObjectId(lastReturnedObjectId);
			
			List<LoyaltyFactorResponseDto> response = this.restClient.requestLoyaltyData(request);
			
			if(!response.isEmpty()) {
				for(int i = 0; i < response.size(); i++) {
					if(i == response.size() - 1) {
						this.lastReturnedObjectId = response.get(i).getLastReturnedObjectId();
						break;
					}
					this.loyaltyFactorDao.save(new LoyaltyFactorDto(response.get(i).getCardCode(), new BigDecimal(response.get(i).getULoyFac())));
					
				}
			} else {
				syncComplete = true;
			}			
		} while (!syncComplete);		
	}
}
