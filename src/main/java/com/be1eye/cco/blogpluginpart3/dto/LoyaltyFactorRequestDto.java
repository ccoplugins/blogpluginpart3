package com.be1eye.cco.blogpluginpart3.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class LoyaltyFactorRequestDto {
	
	@JsonProperty("SYSID")
	private String sysId;
	@JsonProperty("LastModifiedDate")
	private String lastModifiedDate;
	@JsonProperty("QueryHitsMaximumValue")
	private String queryHitsMaximumValue;
	@JsonProperty("LastReturnedObjectId")
	private String lastReturnedObjectId;
	@JsonProperty("CardCode")
	private String cardCode;

	public String getSysId() {
	return sysId;
	}

	public void setSysId(String sYSID) {
	this.sysId = sYSID;
	}

	public String getLastModifiedDate() {
	return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
	this.lastModifiedDate = lastModifiedDate;
	}

	public String getQueryHitsMaximumValue() {
	return queryHitsMaximumValue;
	}

	public void setQueryHitsMaximumValue(String queryHitsMaximumValue) {
	this.queryHitsMaximumValue = queryHitsMaximumValue;
	}

	public String getLastReturnedObjectId() {
	return lastReturnedObjectId;
	}

	public void setLastReturnedObjectId(String lastReturnedObjectId) {
	this.lastReturnedObjectId = lastReturnedObjectId;
	}

	public String getCardCode() {
	return cardCode;
	}

	public void setCardCode(String cardCode) {
	this.cardCode = cardCode;
	}

}
