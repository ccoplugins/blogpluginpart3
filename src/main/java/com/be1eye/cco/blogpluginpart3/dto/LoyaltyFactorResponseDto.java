package com.be1eye.cco.blogpluginpart3.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class LoyaltyFactorResponseDto {

	@JsonProperty("CardCode")
	private String cardCode;
	@JsonProperty("U_LoyFac")
	private String uLoyFac;
	@JsonProperty("LastReturnedObjectId")
	private String lastReturnedObjectId;

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getULoyFac() {
		return uLoyFac;
	}

	public void setULoyFac(String uLoyFac) {
		this.uLoyFac = uLoyFac;
	}

	public String getLastReturnedObjectId() {
		return lastReturnedObjectId;
	}

	public void setLastReturnedObjectId(String lastReturnedObjectId) {
		this.lastReturnedObjectId = lastReturnedObjectId;
	}
}
