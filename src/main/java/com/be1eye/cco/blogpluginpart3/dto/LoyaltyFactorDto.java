package com.be1eye.cco.blogpluginpart3.dto;

import java.math.BigDecimal;

/**
 * 
 * @author RobertZieschang zieschang@be1eye.de
 *
 */
public class LoyaltyFactorDto {

	private String cardCode;
	
	private BigDecimal loyaltyFactor;
	
	public LoyaltyFactorDto() {}
	
	public LoyaltyFactorDto(String inCardCode, BigDecimal inLoyaltyFactor) {
		this.cardCode = inCardCode;
		this.loyaltyFactor = inLoyaltyFactor;
				
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public BigDecimal getLoyaltyFactor() {
		return loyaltyFactor;
	}

	public void setLoyaltyFactor(BigDecimal loyaltyFactor) {
		this.loyaltyFactor = loyaltyFactor;
	}
	
	@Override
	public String toString() {
		return "LoyaltyFactorDTO [cardCode=" + cardCode + ", loyaltyFactor=" + loyaltyFactor + "]";
	}
}

